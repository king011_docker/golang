#!/usr/bin/env bash
set -e

Command="build.sh"
Version="18.04"
Tag="king011/development:$Version"
RunName="test-dev-$Version"

function display_help
{
    echo "build $Tag"
    echo
    echo "Usage:"
    echo "  $Command [flags]"
    echo
    echo "Flags:"
    echo '  -b,--build          build docker image'
    echo '  -r,--run            run docker container'
    echo "  -h, --help          help for $Command"
}
function build_image
{
    cd "$(dirname "$BASH_SOURCE")"
    set -x
    sudo docker build \
        --network host \
        -t "$Tag" .
}
function run_container
{
    cd "$(dirname "$BASH_SOURCE")"

    set -x
    sudo docker run \
        -u dev \
        --name "$RunName"   \
        --rm -it \
        "$Tag"  bash
}

ARGS=`getopt -o hbr --long help,build,run -n "$Command" -- "$@"`
eval set -- "${ARGS}"
_Build=0
_Run=0

while true
do
    case "$1" in
        -h|--help)
            display_help
            exit 0
        ;;
        -b|--build)
            _Build=1
            shift
        ;;
        -r|--run)
            _Run=1
            shift
        ;;
        --)
            shift
            break
        ;;
        *)
            echo Error: unknown flag "$1" for "$Command"
            echo "Run '$Command --help' for usage."
            exit 1
        ;;
    esac
done

if [[ $_Build == 1 ]];then
    build_image
    exit $?
fi

if [[ $_Run == 1 ]];then
    run_container
    exit $?
fi
display_help
exit 1
