#!/usr/bin/env bash
set -e

Command="build.sh"
Version="1.19.4"
Tag="king011/golang:$Version"
RunName="test-go-$Version"

function display_help
{
    echo "build $Tag"
    echo
    echo "Usage:"
    echo "  $Command [flags]"
    echo
    echo "Flags:"
    echo '  -a,--attach         attach container'
    echo '  -b,--build          build docker image'
    echo '  -d,--delete         delete container'
    echo '  -r,--run            run docker container'
    echo "  -h, --help          help for $Command"
}
function build_image
{
    cd "$(dirname "$BASH_SOURCE")"
    if [ -d cnf.tmp ];then
        rm cnf.tmp -rf
    fi
    cp ../cnf ./cnf.tmp -r
    set -x
    sudo docker build \
        --network host \
        -t "$Tag" .
    rm ./cnf.tmp -rf
}
function attach_container
{
    set -x
    sudo docker exec -u dev -it "$RunName" bash
}
function delete_container
{
    set -x
    sudo docker rm -f "$RunName"
}
function run_container
{
    cd "$(dirname "$BASH_SOURCE")"
    local dir=`pwd`
    local project="$dir/../project"
    set -x
    sudo docker run \
        --name "$RunName"   \
        -p 9000:80/tcp \
        -v "$project":/home/dev/project \
        --privileged \
        -d "$Tag"
}

ARGS=`getopt -o habdr --long help,attach,build,delete,run -n "$Command" -- "$@"`
eval set -- "${ARGS}"
_Attach=0
_Build=0
_Delete=0
_Run=0

while true
do
    case "$1" in
        -h|--help)
            display_help
            exit 0
        ;;
        -a|--attach)
            _Attach=1
            shift
        ;;
        -b|--build)
            _Build=1
            shift
        ;;
        -d|--delete)
            _Delete=1
            shift
        ;;
        -r|--run)
            _Run=1
            shift
        ;;
        --)
            shift
            break
        ;;
        *)
            echo Error: unknown flag "$1" for "$Command"
            echo "Run '$Command --help' for usage."
            exit 1
        ;;
    esac
done


if [[ $_Attach == 1 ]];then
    attach_container
    exit $?
fi

if [[ $_Build == 1 ]];then
    build_image
    exit $?
fi

if [[ $_Delete == 1 ]];then
    delete_container
    exit $?
fi

if [[ $_Run == 1 ]];then
    run_container
    exit $?
fi
display_help
exit 1
