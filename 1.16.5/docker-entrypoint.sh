#!/bin/bash

if [[ "$@" == "watch-default" ]];then
    /opt/watch/watch -conf /opt/watch/list.json
else
    exec gosu dev "$@"
fi