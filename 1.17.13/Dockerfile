FROM king011/development:18.04

# cgo linux amd64
RUN set -eux; \
apt-get update; \
    apt-get install -y --no-install-recommends g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config; \
    rm -rf /var/lib/apt/lists/*; 

# cross windows arm
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends mingw-w64 gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf; \
    rm -rf /var/lib/apt/lists/*; 

# cross mac
RUN set -eux; \
    curl -#Lo /a.tar.gz https://gitlab.com/king011_docker/golang-data/-/raw/main/1.16.5/osxcross.tar.gz; \
    tar -zxvf /a.tar.gz -C /opt; \
    rm /a.tar.gz; 
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends clang llvm-dev uuid-dev libssl-dev libbz2-dev; \
    rm -rf /var/lib/apt/lists/*; 


# jsonnet
# grpc
RUN set -eux; \
    curl -#Lo /a.tar.gz https://github.com/google/jsonnet/releases/download/v0.17.0/jsonnet-bin-v0.17.0-linux.tar.gz; \
    tar -zxvf /a.tar.gz -C /usr/local/bin; \
    chmod a+x /usr/local/bin/jsonnet; \
    chmod a+x /usr/local/bin/jsonnetfmt; \
    curl -#Lo /a.zip https://github.com/protocolbuffers/protobuf/releases/download/v21.5/protoc-21.5-linux-x86_64.zip; \
    unzip -d /pb /a.zip;\
    chmod a+x /pb/bin/protoc; \
    cp /pb/bin/protoc /usr/local/bin/; \
    cp /pb/include/google /usr/local/include/ -r; \
    chmod a+rx /usr/local/include -R; \
    chmod a-x /usr/local/include/google/protobuf/*.proto; \
    chmod a-x /usr/local/include/google/protobuf/compiler/*.proto; \
    rm /a.zip; \
    rm /pb -rf;

# gateway openapiv2
RUN set -eux; \
    curl -#Lo /usr/local/bin/protoc-gen-grpc-gateway https://github.com/grpc-ecosystem/grpc-gateway/releases/download/v2.11.2/protoc-gen-grpc-gateway-v2.11.2-linux-x86_64; \
    curl -#Lo /usr/local/bin/protoc-gen-openapiv2 https://github.com/grpc-ecosystem/grpc-gateway/releases/download/v2.11.2/protoc-gen-openapiv2-v2.11.2-linux-x86_64; \
    chmod a+x /usr/local/bin/protoc-gen-grpc-gateway; \
    chmod a+x /usr/local/bin/protoc-gen-openapiv2;

# Because I am in West Korea, I have no choice but to configure an agent environment, fucking ccp.
# * watch
# * coredns
# * v2ray-web
RUN set -eux; \
    mkdir /opt/watch; \
    curl -#Lo /a.tar.gz https://gitlab.com/king011/fuckccp/uploads/d9ee11b0b28f4eb09d75cdd0f7ca86e2/watch.tar.gz; \
    tar -zxvf /a.tar.gz -C /opt/watch; \
    rm /a.tar.gz; \
    mkdir /opt/coredns; \
    curl -#Lo /a.tar.gz https://github.com/coredns/coredns/releases/download/v1.8.7/coredns_1.8.7_linux_amd64.tgz; \
    tar -zxvf /a.tar.gz -C /opt/coredns; \
    rm /a.tar.gz; \
    mkdir /opt/v2ray-web; \
    curl -#Lo /a.tar.gz https://github.com/zuiwuchang/v2ray-web/releases/download/v1.5.0/linux.amd64.tar.gz; \
    tar -zxvf /a.tar.gz -C /opt/v2ray-web; \
    rm /a.tar.gz;

# golang
RUN set -eux; \
    echo 'go linux/amd64'; \
    curl -#Lo /a.tar.gz https://go.dev/dl/go1.17.13.linux-amd64.tar.gz; \
    mkdir /opt/google; \
    tar -zxvf /a.tar.gz -C /opt/google; \
    rm /a.tar.gz;

# code-server
RUN set -eux; \
    curl -#Lo /a.tar.gz https://github.com/coder/code-server/releases/download/v4.6.0/code-server-4.6.0-linux-amd64.tar.gz; \
    tar -zxvf /a.tar.gz -C /opt/; \
    mv /opt/code-server-4.6.0-linux-amd64 /opt/code-server;\
    rm /a.tar.gz;

# extension
RUN set -eux;\
    gosu dev /opt/code-server/bin/code-server --install-extension heptio.Jsonnet; \
    gosu dev /opt/code-server/bin/code-server --install-extension zxh404.vscode-proto3; \
    gosu dev /opt/code-server/bin/code-server --install-extension golang.Go 

# golang lib
RUN set -eux; \
    mkdir /opt/google/golib; \
    export GOROOT=/opt/google/go; \
    export GOPATH=/opt/google/golib; \
    export PATH=$PATH:/opt/google/go/bin; \
    echo go install  \
            go-outline \
            gotests \
            gomodifytags \
            impl \
            goplay \
            dlv \
            staticcheck \
            gopls \
        ;\
    go install github.com/ramya-rao-a/go-outline@latest; \
    go install github.com/cweill/gotests/gotests@latest; \
    go install github.com/fatih/gomodifytags@latest; \
    go install github.com/josharian/impl@latest; \
    go install github.com/haya14busa/goplay/cmd/goplay@latest; \
    go install github.com/go-delve/delve/cmd/dlv@latest; \
    go install honnef.co/go/tools/cmd/staticcheck@latest; \
    go install golang.org/x/tools/gopls@latest; \
    echo go install grpc;\
    go install github.com/golang/protobuf/protoc-gen-go@latest; \
    go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest; \
    chown dev.dev /opt/google/golib -R;\
    rm /root/.cache -rf

# code-server config
RUN set -eux; \
    echo '.local .config for code-server'; \
    mkdir /home/dev/.config/code-server -p;\
    echo 'bind-addr: 127.0.0.1:8080' > /home/dev/.config/code-server/config.yaml; \
    echo 'auth: password' >> /home/dev/.config/code-server/config.yaml; \
    echo 'password: af3d17c1e792920150cd2d60' >> /home/dev/.config/code-server/config.yaml; \
    echo 'cert: false' >> /home/dev/.config/code-server/config.yaml; \
    chown dev.dev /home/dev/.config -R

# configure proxy env fucking ccp 
COPY cnf.tmp /cnf
RUN set -eux; \
    echo 'configure proxy'; \
    mv /cnf/list.json /opt/watch/; \
    mv /cnf/Corefile /opt/coredns/; \
    mv /cnf/v2ray-web.db /opt/v2ray-web/; \
    mv /cnf/v2ray-web.jsonnet /opt/v2ray-web/; \
    mv /cnf/proxychains.conf /etc/; \
    groupadd -r coredns && useradd -r -g coredns coredns;\
    chown coredns.coredns /opt/coredns/*;\
    groupadd -r v2ray-web && useradd -r -g v2ray-web v2ray-web;\
    chown v2ray-web.v2ray-web /opt/v2ray-web/*;\
    echo 'configure bash_completion'; \
    cat /cnf/bash_completion.sh >> /etc/bash.bashrc; \
    echo 'configure nginx'; \
    cp /cnf/nginx/nginx.conf /etc/nginx/; \
    cp /cnf/nginx/snippets /etc/nginx/ -r; \
    cp /cnf/nginx/conf.d/ /etc/nginx/ -r; \
    rm /cnf -rf;\
    mkdir /home/dev/.cache; \
    chown dev.dev /home/dev/.cache;

# go 環境變量配置
RUN set -eux; \
    mkdir /home/dev/project; \
    chown dev.dev /home/dev/project;\
    echo "export GOROOT=/opt/google/go" >> /etc/profile; \
    echo "export GOPATH=/opt/google/golib" >> /etc/profile; \
    echo "export PATH=\"$PATH:/opt/google/go/bin:/opt/google/golib/bin:/opt/osxcross/bin\"" >> /etc/profile; \
    echo "export GOROOT=/opt/google/go" >> /etc/bash.bashrc; \
    echo "export GOPATH=/opt/google/golib" >> /etc/bash.bashrc; \
    echo "export PATH=\"$PATH:/opt/google/go/bin:/opt/google/golib/bin:/opt/osxcross/bin\"" >> /etc/bash.bashrc;

ENV GOROOT=/opt/google/go \
    GOPATH=/opt/google/golib \
    PATH=$PATH:/opt/google/go/bin:/opt/google/golib/bin:/opt/osxcross/bin 

VOLUME ["/home/dev/project"]


COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 22 80
WORKDIR /home/dev
CMD ["watch-default"]